#include <fstream>
#include <cstring>
#include "iobio.hpp"

using namespace std;
using namespace iobio;

bool PDBReader::next() {	
	if (!getline(*_is, _line)) {
		return false;
	}
	_lineidx += 1;
	return true;
}

PDBRecordType PDBReader::type() const {
	if (_line.compare(0, 7, "HEADER ") == 0) {
		return HeaderRecordType;
	}
	if (_line.compare(0, 7, "TITLE  ") == 0) {
		return TitleRecordType;
	}
	if (_line.compare(0, 7, "REMARK ") == 0) {
		return RemarkRecordType;
	}
	if (_line.compare(0, 7, "DBREF  ") == 0) {
		return DbRefRecordType;
	}
	if (_line.compare(0, 7, "MODEL  ") == 0) {
		return ModelRecordType;
	}
	if (_line.compare(0, 7, "ATOM   ") == 0) {
		return AtomRecordType;
	}
	if (_line.compare(0, 7, "HETATM ") == 0) {
		return HetAtmRecordType;
	}
	if (_line.compare(0, 7, "END    ") == 0) {
		return EndRecordType;
	}

	return UnknownRecordType;
}

PDBDbRef::PDBDbRef(const PDBDbRefRecord &record) {
	_idcode = record.idcode();
	_chain = record.chain();
	_seqbegin = record.seqbegin();
	_insertbegin = record.insertbegin();
	_seqend = record.seqend();
	_insertend = record.insertend();
	_database = record.database();
	_dbaccession = record.dbaccession();
	_dbidcode = record.dbidcode();
	_dbseqbegin = record.dbseqbegin();
	_idbnsbeg = record.idbnsbeg();
	_dbseqend = record.dbseqend();
	_dbinsend = record.dbinsend();
}

PDBAtom::PDBAtom(const PDBAtomRecord &record) {
	_serial = record.serial();
	_name = record.name();
	_altloc = record.altloc();
	_resname = record.resname();
	_chain = record.chain();
	_resnum = record.resnum();
	_icode = record.icode();
	_x = record.x();
	_y = record.y();
	_z = record.z();
	_occupancy = record.occupancy();
	_tempfactor = record.tempfactor();
	_element = record.element();
	_charge = record.charge();
}

PDBFile::PDBFile(istream *is) {
	PDBReader reader(is);

	while (reader.next()) {
		switch(reader.type()) {
			case DbRefRecordType: {
				PDBDbRef d(reader.as_dbref());
				_dbrefs.push_back(d);
				break;
			}
			case AtomRecordType: {								
				PDBAtom a(reader.as_atom());
				_atoms.push_back(a);
				break;
			}
		}
	}		
}

const vector<PDBAtom> PDBFile::backbone() const {
	vector<PDBAtom> aux;
	for (auto a: _atoms) {
		bool is_backbone = a.name() == " CA " || a.name() == "  C " || a.name() == "  N " || a.name() == "  O ";
		if (is_backbone) {
			aux.push_back(a);
		}
	}
	return aux;
}

const vector<PDBAtom> PDBFile::centroids() const {
	vector<PDBAtom> aux;
	for (auto a: _atoms) {
		bool is_centroid = (a.resname() == "GLY" && a.name() == " CA ") || a.name() == " CB ";
		if (is_centroid) {
			aux.push_back(a);
		}
	}
	return aux;
}