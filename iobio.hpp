#ifndef IOBIO_H
#define IOBIO_H

#include "string"
#include "istream"
#include "vector"
#include <iostream>
#include <fstream>

namespace iobio {

	enum PDBRecordType { UnknownRecordType, HeaderRecordType, TitleRecordType,
		RemarkRecordType, DbRefRecordType, ModelRecordType, AtomRecordType, 
		HetAtmRecordType, EndRecordType };

	class PDBDbRefRecord {
	private:
		std::string _s;
	public:
		PDBDbRefRecord() {};
		PDBDbRefRecord(const std::string &s): _s(s) {};			
		PDBDbRefRecord(const PDBDbRefRecord &other): _s(other._s) {};
		~PDBDbRefRecord() {}

		inline std::string idcode() const 		{ return _s.substr(7, 4); };
		inline char chain() const 				{ return _s.substr(12, 1).at(0); };
		inline int seqbegin() const 			{ return stoi(_s.substr(14, 4)); };
		inline char insertbegin() const 		{ return _s.substr(18, 1).at(0); };
		inline int seqend() const 				{ return stoi(_s.substr(20, 4)); };
		inline char insertend() const 			{ return _s.substr(24, 1).at(0); };
		inline std::string database() const 	{ return _s.substr(26, 6); };
		inline std::string dbaccession() const 	{ return _s.substr(33, 8); };
		inline std::string dbidcode() const 	{ return _s.substr(42, 12); };
		inline int dbseqbegin() const			{ return stoi(_s.substr(55, 5)); };
		inline char idbnsbeg() const 			{ return _s.substr(60, 1).at(0); };
		inline int dbseqend() const 			{ return stoi(_s.substr(62, 5)); };
		inline char dbinsend() const 			{ return _s.substr(67, 1).at(0); };
	};

	class PDBAtomRecord {
		private:
			std::string _s;
		public:

		PDBAtomRecord() {};
		PDBAtomRecord(const std::string &s): _s(s) {};			
		PDBAtomRecord(const PDBAtomRecord &other): _s(other._s) {};
		~PDBAtomRecord() {}

		inline int serial() const 			{ return stoi(_s.substr(6, 5)); }
		inline std::string name() const 	{ return _s.substr(12, 4); }
		inline char altloc() const 			{ return _s.substr(16, 1).at(0); }
		inline std::string resname() const 	{ return _s.substr(17, 3); }
		inline char chain() const 			{ return _s.substr(21, 1).at(0); }
		inline int resnum() const 			{ return stoi(_s.substr(22, 4)); }
		inline char icode() const 			{ return _s.substr(26, 1).at(0); }
		inline float x() const 				{ return stof(_s.substr(30, 8)); }
		inline float y() const 				{ return stof(_s.substr(38, 8)); }
		inline float z() const 				{ return stof(_s.substr(46, 8)); }
		inline float occupancy() const 		{ return stof(_s.substr(54, 6)); }
		inline float tempfactor() const 	{ return stof(_s.substr(60, 6)); }
		inline std::string element() const 	{ return _s.size() > 66 ? _s.substr(76, 2) : ""; }
		inline std::string charge() const 	{ return _s.size() > 78 ? _s.substr(78, 2) : ""; }
	};


	class PDBReader {
		//TODO transform in an iterator		

		//class iterator {

		//}
		private:
			std::istream *_is;
			std::string _line;
			int _lineidx;
		public:
			PDBReader(std::istream *is): _is(is), _lineidx(0) { };

			bool next();
			// begin()
			// end()

			inline const std::string &line() const { return _line; }
			inline int lineidx() const { return _lineidx; }
			PDBRecordType type() const;
			inline PDBDbRefRecord as_dbref() const { return PDBDbRefRecord(_line); }
			inline PDBAtomRecord as_atom() const { return PDBAtomRecord(_line); }
	};

	class PDBDbRef {
	private:
		std::string _idcode;
		char _chain;
		int _seqbegin;
		char _insertbegin;
		int _seqend;
		char _insertend;
		std::string _database;
		std::string _dbaccession;
		std::string _dbidcode;
		int _dbseqbegin;
		char _idbnsbeg;
		int _dbseqend;
		char _dbinsend;
	public:
		PDBDbRef();
		PDBDbRef(const PDBDbRefRecord &record);
		~PDBDbRef() { }
		std::string to_record();

		inline std::string idcode() const 		{ return _idcode; }
		inline char chain() const 				{ return _chain; }
		inline int seqbegin() const 			{ return _seqbegin; }
		inline char insertbegin() const 		{ return _insertbegin; }
		inline int seqend() const 				{ return _seqend; }
		inline char insertend() const 			{ return _insertend; }
		inline std::string database() const 	{ return _database; }
		inline std::string dbaccession() const 	{ return _dbaccession; }
		inline std::string dbidcode() const 	{ return _dbidcode; }
		inline int dbseqbegin() const 			{ return _dbseqbegin; }
		inline char idbnsbeg() const 			{ return _idbnsbeg; }
		inline int dbseqend() const 			{ return _dbseqend; }
		inline char dbinsend() const 			{ return _dbinsend; }
	};

	class PDBAtom {
	private:
		int _serial;
		std::string _name;
		char _altloc;
		std::string _resname;
		char _chain;
		int _resnum;
		char _icode;
		float _x;
		float _y;
		float _z;
		float _occupancy;
		float _tempfactor;
		std::string _element;
		std::string _charge;
	public:
		PDBAtom();
		PDBAtom(const PDBAtomRecord &record);
		~PDBAtom() { }
		std::string to_record();

		inline int serial() const 			{ return _serial; }
		inline std::string name() const 	{ return _name; }
		inline char altloc() const 			{ return _altloc; }
		inline std::string resname() const 	{ return _resname; }
		inline char chain() const 			{ return _chain; }
		inline int resnum() const 			{ return _resnum; }
		inline char icode() const 			{ return _icode; }
		inline float x() const 				{ return _x; }
		inline float y() const 				{ return _y; }
		inline float z() const 				{ return _z; }
		inline float occupancy() const 		{ return _occupancy; }
		inline float tempfactor() const 	{ return _tempfactor; }
		inline std::string element() const 	{ return _element; }
		inline std::string charge() const 	{ return _charge; }
	};

	class PDBFile {
	private:
		std::vector<PDBAtom> _atoms;			
		std::vector<PDBDbRef> _dbrefs;
	public:
		PDBFile(std::istream *is);
		~PDBFile() {};

		inline const std::vector<PDBDbRef> dbrefs() const { return _dbrefs; };
		inline const std::vector<PDBAtom> atoms() const { return _atoms; };
		const std::vector<PDBAtom> backbone() const;
		const std::vector<PDBAtom> centroids() const;
		const std::vector<int> models() const;
		const std::vector<char> chains() const;
		const std::vector<std::string> resnames() const;
		const std::vector<int> resnums() const;
	};

	class PDBWriter {
	public:
		PDBWriter(std::ostream *os);
		PDBWriter(std::string filename);
		~PDBWriter() {};
		
		void putAtom(/*atom fields*/);
		void putModel(/*model fields*/);
	};


}

#endif