#include "iobio.hpp"
#include <fstream>
#include <iostream>
#include <string>

using namespace std;
using namespace iobio;

void echo(string filename) {
	try {
		ifstream ifs(filename);
		PDBReader f(&ifs);
		while (f.next()) {
			auto type = f.type();

			cout << "line_idx=" << f.lineidx() 
				<< " type=" << static_cast<int>(type) 
				<< " line=" << f.line() 
				<< endl;
		}
	} catch (fstream::failure &e) {
		cout << "Exception opening " << filename << e.what() << endl;
	}
}

void print_atom(PDBAtomRecord &a) {
	cout << 
		"serial=" << a.serial() << " "
		"name=" << a.name() << " "
		"altloc=" << a.altloc() << " "
		"resname=" << a.resname() << " "
		"chain=" << a.chain() << " "
		"resnum=" << a.resnum() << " "
		"icode=" << a.icode() << " "
		"x=" << a.x() << " "
		"y=" << a.y() << " "
		"z=" << a.z() << " "
		"occupancy=" << a.occupancy() << " "
		"tempfactor=" << a.tempfactor() << " "
		"element=" << a.element() << " "
		"charge=" << a.charge()
	<< endl;
}

void print_atom(PDBAtom &a) {
	cout << 
		"PDBAtom " << 
		"serial=" << a.serial() << " "
		"name=" << a.name() << " "
		"altloc=" << a.altloc() << " "
		"resname=" << a.resname() << " "
		"chain=" << a.chain() << " "
		"resnum=" << a.resnum() << " "
		"icode=" << a.icode() << " "
		"x=" << a.x() << " "
		"y=" << a.y() << " "
		"z=" << a.z() << " "
		"occupancy=" << a.occupancy() << " "
		"tempfactor=" << a.tempfactor() << " "
		"element=" << a.element() << " "
		"charge=" << a.charge()
	<< endl;
}

void read_atoms(string filename) {
	try {
		ifstream ifs(filename);
		PDBReader f(&ifs);
		while (f.next()) {
			auto type = f.type();
			if (type == AtomRecordType) {
				PDBAtomRecord a = f.as_atom();				
				print_atom(a);
			}
		}
	} catch (...) {
		cout << "Exception ocurred " << endl;
	}
}

void pdb_file(string filename) {
	ifstream ifs(filename);
	PDBFile f(&ifs);
	for (auto a: f.atoms()) {
		print_atom(a);
	}
}

int main(int argc, char const *argv[]) {
	cout << "PSE" << endl;
	if (argc < 2) {
		cout << "usage: pse FILE" << endl;
		return 1;
	}
	cout << "argv[1] = " << argv[1] << endl;
	string filename = string(argv[1]);

	//echo(filename);
	//read_atoms(filename);
	pdb_file(filename);

	return 0;
}